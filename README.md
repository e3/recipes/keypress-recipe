# keypress conda recipe

Home: "https://github.com/paulscherrerinstitute/keypress"

Package license: none

Recipe license: BSD 3-Clause

Summary: EPICS keypress module
